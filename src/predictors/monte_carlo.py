from contextlib import contextmanager
from dataclasses import dataclass
from statistics import mean, stdev

import torch
import torch.nn as nn

from src.predictors.predictor import PneumoniaPredictor, Prediction
from src.settings import THRESHOLD


@contextmanager
def switch_eval_mode(model):
    model.train()
    yield
    model.eval()


@dataclass
class BayesianPrediction(Prediction):
    uncertainty: float


class MonteCarloPneumoniaPredictor(PneumoniaPredictor):
    DEFAULT_SAMPLES = 10

    def __init__(self, nn_model, n_samples=DEFAULT_SAMPLES, threshold=THRESHOLD):
        super().__init__(nn_model, threshold)
        self.n_samples = n_samples
        self.input_dropout = nn.Dropout(0.1)

    def forward(self, x):
        net_outputs = []
        x = self.pre_process(x)
        x = x.to(self.device)
        with torch.no_grad(), switch_eval_mode(self.model):
            for _ in range(self.n_samples):
                net_input = self.input_dropout(x)
                net_outputs.append(self.model.predict(net_input).cpu().numpy().item())
        mean_output, std_dev_output = mean(net_outputs), stdev(net_outputs)
        return mean_output, std_dev_output

    def predict(self, x):
        mean_output, std_dev_output = self.forward(x)
        prediction = mean_output >= self.threshold
        return BayesianPrediction(prediction, mean_output, std_dev_output)
