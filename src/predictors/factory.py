import os

import torch

from src.models.conv5_model import Conv5Model
from src.predictors.monte_carlo import MonteCarloPneumoniaPredictor


def build_monte_carlo_predictor(model_name, model_path):
    if model_name == 'conv-5':
        model_cls = Conv5Model
    else:
        raise ValueError(f"Invalid model name - not available: {model_name}")

    model = _load_trained_model(model_cls, model_path)
    return MonteCarloPneumoniaPredictor(model, n_samples=10, threshold=0.5)


def _load_trained_model(model_cls, model_path):
    assert os.path.isfile(model_path)
    model = model_cls()
    model.load_state_dict(torch.load(model_path))
    return model
