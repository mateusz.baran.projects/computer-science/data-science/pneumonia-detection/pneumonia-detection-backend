from abc import abstractmethod
from dataclasses import dataclass

import torch
from captum.attr import IntegratedGradients
from captum.attr import visualization as viz
from PIL import Image
from torch import nn
from torchvision import transforms

from src.settings import DEVICE, THRESHOLD


@dataclass
class Prediction:
    pneumonia: int
    confidence: float


class PneumoniaPredictor(nn.Module):
    def __init__(self, nn_model, threshold=THRESHOLD, device=DEVICE):
        super().__init__()
        self.model = nn_model
        self.threshold = threshold
        self.device = device

        self.model.eval()
        self.model.to(device)

    def __call__(self, *args, **kwargs):
        return self.predict(*args, **kwargs)

    def pre_process(self, x: Image):
        width, height = self.model.input_size
        x = x.resize((width, height)).convert('L')
        x = transforms.ToTensor()(x).reshape(-1, 1, width, height)
        return x

    def forward(self, x: Image):
        x = self.pre_process(x)
        x = x.to(self.device)
        with torch.no_grad():
            net_out = self.model.predict(x).cpu().numpy().item()
        return net_out

    @abstractmethod
    def predict(self, x):
        """Runs model and returns binary prediction"""
        confidence = self.forward(x)
        prediction = confidence >= self.threshold
        return Prediction(prediction, confidence)

    def explain(self, x):
        x = self.pre_process(x)
        x = x.to(self.device)
        x.requires_grad = True
        ig = IntegratedGradients(self.model)
        attribution, *_ = ig.attribute(x)
        attribution = attribution.permute(1, 2, 0).cpu().detach().numpy()
        x = x.cpu().detach()[0].permute(1, 2, 0)
        x = x.numpy()
        fig, ax = viz.visualize_image_attr(attribution, x, "blended_heat_map", cmap='Reds',
                                           use_pyplot=False)
        return fig
