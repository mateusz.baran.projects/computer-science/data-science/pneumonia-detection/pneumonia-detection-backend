from time import time

import numpy as np
import torch
from sklearn.metrics import accuracy_score, f1_score


def train(model, criterion, optimizer, device, training_loader, val_loader, epochs=10, writer=None,
          threshold=0.5, verbose=True):
    start = time()
    model.to(device)
    for epoch in range(epochs):
        model.train()
        for inputs, labels in training_loader:
            inputs, labels = inputs.to(device), labels.to(device)

            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

        train_pred, train_true, train_loss = evaluate(model, training_loader, device, criterion,
                                                      threshold)
        train_f1, train_acc = get_metrics(train_true.cpu().numpy(), train_pred.cpu().numpy())

        val_pred, val_true, val_loss = evaluate(model, val_loader, device, criterion, threshold)
        val_f1, val_acc = get_metrics(val_true.cpu().numpy(), val_pred.cpu().numpy())

        if writer is not None:
            writer.add_scalar('Loss/train', train_loss, global_step=epoch)
            writer.add_scalar('Loss/val', val_loss, global_step=epoch)
            writer.add_scalar('F-score/train', train_f1, global_step=epoch)
            writer.add_scalar('F-score/val', val_f1, global_step=epoch)
            writer.add_scalar('Accuracy/train', train_acc, global_step=epoch)
            writer.add_scalar('Accuracy/val', val_acc, global_step=epoch)

        if verbose:
            print('########################################')
            print('## Epoch:', epoch)
            print('## Loss/train:', train_loss)
            print('## Loss/val:', val_loss)
            print('## F-score/train:', train_f1)
            print('## F-score/val:', val_f1)
            print('## Accuracy/train:', train_acc)
            print('## Accuracy/val:', val_acc)
            print('## Running time:', time() - start)
            print('########################################')


def evaluate(model, loader, device, criterion=None, threshold=0.5):
    y_pred = []
    y_true = []
    losses = []

    model.to(device)
    model.eval()
    with torch.no_grad():
        for features, labels in loader:
            features, labels = features.to(device), labels.to(device)
            outputs, logits = model.predict(features, with_logits=True)
            y_pred.append(outputs >= threshold)
            y_true.append(labels)

            if criterion is not None:
                losses.append(criterion(logits, labels).cpu().item())

    if criterion is not None:
        return torch.cat(y_pred).view(-1, 1), torch.cat(y_true).view(-1, 1), np.mean(losses)
    return torch.cat(y_pred).view(-1, 1), torch.cat(y_true).view(-1, 1)


def get_metrics(y_true, y_pred):
    f1 = f1_score(y_true, y_pred)
    acc = accuracy_score(y_true, y_pred)
    return f1, acc
