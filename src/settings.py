import os

import torch

PROJECT_DIR = os.path.normpath(os.path.join(os.path.dirname(__file__), ".."))

DATA_DIR = os.path.join(PROJECT_DIR, 'data')
RESULT_DIR = os.path.join(PROJECT_DIR, 'results')
MODELS_DIR = os.path.join(RESULT_DIR, 'models')
WRITER_LOG_DIR = os.path.join(RESULT_DIR, 'logs', 'tensorboard')

DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'

THRESHOLD = 0.5

try:
    from src.user_settings import *
except ImportError:
    pass
