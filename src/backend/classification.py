import base64
import io
import os
from io import BytesIO

from PIL import Image

from src.predictors.factory import build_monte_carlo_predictor
from src.settings import MODELS_DIR

model_path = os.path.join(MODELS_DIR, 'cnn_5_epoch_0.pth')
predictor = build_monte_carlo_predictor('conv-5', model_path)


def perform_classification(image):
    orig_width, orig_height = image.size
    res = predictor.predict(image)
    _, confidence, uncertainty = res.pneumonia, res.confidence, res.uncertainty
    explanation_fig = predictor.explain(image)
    image = _fig_to_pil(explanation_fig, orig_width, orig_height)
    return confidence, uncertainty, image


def classify_image(image):
    mean, variance, heatmap = perform_classification(image)
    data_uri = get_data_uri(heatmap)

    return {
        'mean': mean,
        'variance': variance,
        'heatmap': data_uri
    }


def get_data_uri(image):
    buffered = BytesIO()
    image.save(buffered, format="JPEG")
    img_str = base64.b64encode(buffered.getvalue())
    return f'data:image/jpeg;base64,{img_str.decode()}'


def _fig_to_pil(matplot_fig, width=None, height=None):
    buffer = io.BytesIO()
    matplot_fig.savefig(buffer, format='JPEG')
    buffer.seek(0)
    image = Image.open(buffer)
    if width is not None and height is not None:
        image = image.resize((width, height))
    return image
