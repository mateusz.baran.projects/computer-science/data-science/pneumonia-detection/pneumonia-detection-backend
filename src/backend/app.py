from flask import Flask, jsonify, request
from flask_api import status
from flask_cors import CORS
from PIL import Image

from src.backend.classification import classify_image

app = Flask(__name__)
ALLOWED_EXTENSIONS = {'jpg', 'jpeg'}
CORS(app, resources={r"/classify": {"origins": "*"}})


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/classify', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            response = {'Error': 'Request should contain image file to classify'}
            return response, status.HTTP_400_BAD_REQUEST
        file = request.files['file']
        if file and allowed_file(file.filename):
            img = Image.open(file)
            classification_results = classify_image(img)
            return jsonify(classification_results)


if __name__ == '__main__':
    app.run(host='localhost', port=5000)
