import os
from datetime import datetime

import torch
import torch.optim as optim
from torch import nn
from torch.backends import cudnn
from torch.utils import data
from torch.utils.tensorboard import SummaryWriter

from src.data_processing.chest_x_ray import create_dataset
from src.datasets.make_dataset import Dataset
from src.learning.train import evaluate, get_metrics, train
from src.models.conv5_model import Conv5Model
from src.settings import MODELS_DIR, WRITER_LOG_DIR

dataset_params = {
    'batch_size': 64,
    'shuffle': True,
    'num_workers': 6,
}

training_params = {
    'max_epochs': 15,
    'threshold': .5,
}

model = Conv5Model()
criterion = nn.BCEWithLogitsLoss()
optimizer = optim.Adam(model.parameters())

use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
cudnn.benchmark = True

name = datetime.now().strftime("%Y%m%d-%H%M%S")
writer = SummaryWriter(os.path.join(WRITER_LOG_DIR, name))

(x_train, y_train), (x_val, y_val), (x_test, y_test) = create_dataset(model.input_size)

training_set = Dataset(x_train, y_train, True)
train_drop_last = len(training_set) >= dataset_params['batch_size']
training_generator = data.DataLoader(training_set, **dataset_params, drop_last=train_drop_last)

val_set = Dataset(x_val, y_val)
val_drop_last = len(val_set) >= dataset_params['batch_size']
val_generator = data.DataLoader(val_set, **dataset_params, drop_last=val_drop_last)

test_set = Dataset(x_test, y_test)
test_generator = data.DataLoader(test_set, **dataset_params)

print('########################################')
print('## Start training model:', name)
print('########################################')

try:
    train(model, criterion, optimizer, device, training_generator,
          val_generator, training_params['max_epochs'], writer=writer, verbose=True)
except KeyboardInterrupt:
    print('########################################')
    print('## Stopping...')
    print('########################################')

y_pred, y_true, test_loss = evaluate(model, test_generator, device, criterion,
                                     training_params['threshold'])
test_f1, test_acc = get_metrics(y_true.cpu().numpy(), y_pred.cpu().numpy())

writer.add_hparams({**dataset_params, **training_params}, {
    'Loss/test': test_loss,
    'F-score/test': test_f1,
    'Accuracy/test': test_acc,
})

writer.close()

model_save_path = os.path.join(MODELS_DIR, f'model-{name}.pt')
print('########################################')
print('## Saving model:', model_save_path)
print('########################################')
torch.save(model.state_dict(), model_save_path)
