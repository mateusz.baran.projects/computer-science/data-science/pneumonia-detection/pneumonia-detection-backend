import torch
from torch.utils import data
from torchvision import transforms


class Dataset(data.Dataset):
    def __init__(self, x, y, augmentation=False):
        self.x = torch.from_numpy(x).float()
        self.y = torch.from_numpy(y).float()
        self.augmentation = augmentation
        self.transforms = transforms.Compose([
            transforms.ToPILImage(),
            transforms.RandomAffine(degrees=5,
                                    translate=(.1, 0.1),
                                    scale=(0.9, 1.1)),
            transforms.ToTensor()
        ])

    def __len__(self):
        return self.x.shape[0]

    def __getitem__(self, index):
        x = self.x[index]
        if self.augmentation:
            x = self.transforms(x)
        return x, self.y[index]
