def normalize_features(x):
    return x / 255


def reshape(x, y, img_size):
    return x.reshape(-1, 1, *img_size), y.reshape(-1, 1)
