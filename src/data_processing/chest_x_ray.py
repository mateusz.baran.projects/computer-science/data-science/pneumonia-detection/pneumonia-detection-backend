import os

import numpy as np
from PIL import Image
from tqdm.auto import tqdm

from src.data_processing.utils import normalize_features, reshape
from src.settings import DATA_DIR


def load_pil_data(data_dir, img_size):
    labels = ['NORMAL', 'PNEUMONIA']
    data = []
    for label in labels:
        path = os.path.join(data_dir, label)
        class_num = labels.index(label)
        for img in tqdm(os.listdir(path)):
            try:
                image = Image.open(os.path.join(path, img)).convert('L')
                resized_arr = image.resize(img_size)
                data.append([resized_arr, class_num])
            except Exception as e:
                print(e)

    return data


def _create_dataset(data_dir, img_size):
    data = load_pil_data(data_dir, img_size)

    x = []
    y = []

    for feature, label in data:
        x.append(np.array(feature))
        y.append(label)

    x = np.asarray(x)
    y = np.asarray(y)

    x = normalize_features(x)
    x, y = reshape(x, y, img_size)

    return x, y


def create_dataset(img_size):
    return (
        _create_dataset(os.path.join(DATA_DIR, 'raw', 'train'), img_size),
        _create_dataset(os.path.join(DATA_DIR, 'raw', 'val'), img_size),
        _create_dataset(os.path.join(DATA_DIR, 'raw', 'test'), img_size),
    )
