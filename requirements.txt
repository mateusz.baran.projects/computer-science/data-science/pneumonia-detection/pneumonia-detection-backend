torch==1.4.0
torchvision==0.5.0
scikit-learn==0.22.1
tqdm==4.45.0
opencv-python==4.2.0.34
seaborn==0.10.0
matplotlib==3.2.1
pandas==1.0.3
numpy==1.18.3
ipywidgets==7.5.1
Flask==1.1.2
Flask-Cors==3.0.8
Flask-API==2.0
Pillow==7.1.2
tensorboard==2.2.1
captum==0.2.0
